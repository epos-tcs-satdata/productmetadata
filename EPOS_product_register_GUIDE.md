# EPOS_product_register.sh GUIDE - v1.0

<a name="Installation"></a>
## Installation and configuration

<a name="Step1"></a>
### Step 1. Installation
Installation of the software (for Linux/Unix):
 - clone the project:
     `git clone https://gitlab.com/epos-tcs-satdata/productmetadata.git`
 - assign execution permission to the registration script:
     `chmod +x EPOS_product_register.sh`

<a name="Step2"></a>
### Step 2. Account creation
To publish data into the GEP-EPOS catalogue, you need an account on the Terradue portal and be authorized to publish into the GEP catalogue.
If you do not have an account, you can self register via https://www.terradue.com/portal/signup, then send an email to contact@geohazards-tep.eu to request permission for publishing data into the GEP-EPOS catalogue (do not forget to specify your username). If applicable, your inquiry account will be authorized.

<a name="Step3"></a>
### Step 3. API key  generation
To publish data into the GEP-EPOS catalogue, you will need to setup an API key to authenticate to the Terradue platform. To generate one you need to:
 - Login into https://www.terradue.com/portal/ 
 - Go to "Profile" (from "Access your profile" or from the top-right button displayiing your username)
 - Go to "API key" (in the left menu)
 - Press the "Generate a new API Key" button
 - Press the "copy to clipboard" button (second button on the left of "Generate a new API Key"). Your API Key is now into your clipboard.

<a name="Step4"></a>
### Step 4. Credential configuration
Setup your username and API key into the software, by executing the following command on the bash command line:
  `echo 'machine catalog.terradue.com login <your_username> secret <your_apikey>' > EPOS_product_register.credentials`
where `<your_username>` is your registration username and `<your_apikey>` is the API key generated in [Step 2](#Step2).

<a name="Usage"></a>
## Usage 
The application is executed via the following command:
 `./EPOS_product_register.sh <metadata_file>`
where `<metadata_file>` is a unix text file containing the metadata for the product you want to ingest in the format: `< TAG: | Value >`. 
Sample of metadata file structure is provided in **[Table 1](#Table1)**. Further samples of the metadata files can be found into the [samples](https://gitlab.com/epos-tcs-satdata/productmetadata/-/tree/master/samples) folder. You can copy and edit them according to your needs.

**NOTE: Please refer to [Table 2](https://docs.google.com/spreadsheets/d/1ap2ymZFGz-zTiXsttUKfRKDFB5GIOjiRgXc2wnFjg4E/edit#gid=71957665) for a full list of accepted metadata.**

Upon correct execution, the software will return:
  `{"added":1,"updated":0,"deleted":0,"errors":0,"items":[{"id":"InW_CNRIREA_20210508_20210526_T41R","type":"gtfeature","operation":"Add"},{"id":"general","type":"meta","operation":"Update"}]`
for new products
  `{"added":0,"updated":1,"deleted":0,"errors":0,"items":[{"id":"InW_CNRIREA_20210508_20210526_T41R","type":"gtfeature","operation":"Update"},{"id":"general","type":"meta","operation":"Update"}]`
for products already existing (which metadata have been modified)

If the metadata file does not contain all the metadata specified in the sample file, the software will generate a warning, indicating the missing metadata.
If the missing metadata is a mandatory one (like the **Product_ID**), the software will fail (you will see an empty response and error code different from 0).
It is suggested to try to fill most of the metadata (also for coherence towards the TCS) 
If the metadata file contains more metadata than the ones required by the TCS, a warning message will be specified and the metadata will be ignored.

**NOTE: Do not delete or edit the contents of the "templates" folder. This folder is necessary for the correct execution of the application**

<a name="Table1"></a>
## Table 1: Sample of metadata input file for WRAPPED_INTERFEROGRAM product type

| TAG: | Value |
| ------ | ------ |
| DDSS_ID: | WRAPPED_INTERFEROGRAM |
| Product_ID: | InW_CNRIREA_20210508_20210526_T41R |
|Product_format:| GEOTIFF  |
|Product_size:| 485428418|
|Preview_url:| https://store.terradue.com/gep-epos-datarepo/EPOSAR/S1/EQ_US_us7000e54r_Southern_Qinghai_China_7.4/Track_99/Interf/08052021S1A_26052021S1B/InW_CNRIREA_20210508_20210526_T41R/InW_CNRIREA_20210508_20210526_T41R.png|
|Legend_url:| https://store.terradue.com/gep-epos-datarepo/EPOSAR/S1/EQ_US_us7000e54r_Southern_Qinghai_China_7.4/Track_99/Interf/08052021S1A_26052021S1B/InW_CNRIREA_20210508_20210526_T41R/InW_CNRIREA_20210508_20210526_T41R.legend.png|
|Product_url:| https://store.terradue.com/gep-epos-datarepo/EPOSAR/S1/EQ_US_us7000e54r_Southern_Qinghai_China_7.4/Track_99/Interf/08052021S1A_26052021S1B/InW_CNRIREA_20210508_20210526_T41R/InW_CNRIREA_20210508_20210526_T41R.zip|
|Bounding_box:| 36.415278 96.761667 32.589167 100.08528|
|Bounding_box_wkt:| POLYGON((32.589167 96.761667,36.415278 96.761667,36.415278 100.08528,32.589167 100.08528,32.589167 96.761667))|
|License:| https://creativecommons.org/licenses/by/4.0|
|User_ID:| CNRIREA|
|Software_version:| CNR-IREA P-SBAS 30|
|Applied_algorithm_description:| Parallel SBAS Interferometry Chain|
|Main_reference:| 10.1109/TGRS.2002.803792, 10.1109/JSTARS.2014.2322671|
|Date_of_measurement_start:| 2021-05-08T11:26:11.039846Z|
|Date_of_measurement_end:| 2021-05-26T11:25:25.449524Z|
|Date_of_production:| 2021-05-26T22:30:40Z|
|Service_used_for_generation:| EPOSAR|
|Geographic_CS_type_code:| EPSG_4326|
|Used_DEM:| SRTM_1arcsec|
|Super_master_SAR_image_ID:| S1A_IW_SLC__1SDV_20210508T112611_20210508T112637_037796_047601_CA8B.SAFE, S1A_IW_SLC__1SDV_20210508T112635_20210508T112703_037796_047601_ADE1.SAFE, S1A_IW_SLC__1SDV_20210508T112701_20210508T112728_037796_047601_E3E0.SAFE|
|Master_SAR_image_ID:| S1A_IW_SLC__1SDV_20210508T112611_20210508T112637_037796_047601_CA8B.SAFE, S1A_IW_SLC__1SDV_20210508T112635_20210508T112703_037796_047601_ADE1.SAFE, S1A_IW_SLC__1SDV_20210508T112701_20210508T112728_037796_047601_E3E0.SAFE|
|Slave_SAR_image_ID:| S1B_IW_SLC__1SDV_20210526T112525_20210526T112555_027075_033C0F_3031.SAFE, S1B_IW_SLC__1SDV_20210526T112553_20210526T112628_027075_033C0F_4B05.SAFE|
|Perpendicular_baseline:| -45.8456|
|Parallel_baseline:| -31.8638|
|Along_track_baseline:| -3.77986|
|Map_of_LOS_vector:| https://store.terradue.com/gep-epos-datarepo/EPOSAR/S1/EQ_US_us7000e54r_Southern_Qinghai_China_7.4/Track_99/LOS_vector/CosNEU_CNRIREA_75KI.zip|
|Spatial_resolution:| 73, 73|
|Sensor:| S1|
|Mode:| IW|
|Antenna_side:| Right|
|Relative_orbit_number:| 99|
|Orbit_direction:| ASCENDING|
|Wavelenght:| 0.055465760|
|Value_unit:| rad|
|Number_of_looks_azimuth:| 5|
|Number_of_looks_range:| 20|
|Applied_filter:| No_Filter|

