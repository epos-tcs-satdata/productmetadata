#!/bin/bash
 
function usage(){
  echo "EPOS Product register script version $VERSION

Usage: 
  ./${##*/0} [options] <metadata_file>
  
Options are:
  --dry-run  Do not post to the catalog but post the metadata on the standard output
  --replace  Force delete of old entries and then recreate a new one (default is to only update)
  --delete   Delete ingested metadata entry
  --wms      Consider WMS template folder for correct preview visualization in GEP
"
  exit 0
}

function error(){
  echo "[ERROR]: $2"
  echo "FAILED"
  exit $1
}

function warning(){
  echo "[WARNING]: $1"
}

function meta_check(){

  meta=$1
  echo " "
  echo "****************************************"
  echo "Checking $meta file..."


  mandatory_list=("DDSS_ID" "Product_ID" "Product_url" "Bounding_box" "Date_of_measurement_start" "Date_of_measurement_end" "Wavelength")

  for i in ${mandatory_list[@]}; do 
  mandatory_tmp=$(sed -n "s|^$i\(.*\)|\1|gp" < $meta)
    if [ -z "$mandatory_tmp" ]; then
      echo "[ERROR]: Missing mandatory metadata - $i"
      err=12
    fi
  done

  view_list=("Preview_url" "Legend_url")
  ed=$(sed -n "s|^DDSS_ID: \(.*\)|\1|gp" < $meta)
  if [ $ed == "WRAPPED_INTERFEROGRAM" -o $ed == "UNWRAPPED_INTERFEROGRAM" -o $ed == "SPATIAL_COHERENCE" -o $ed == "LOS_DISPLACEMENT_TIMESERIES" ]; then
    for i in ${view_list[@]}; do
      view_tmp=$(sed -n "s|^$i: \(.*\)|\1|gp" < $meta)
      if [ -z "$view_tmp" ]; then
        echo "[ERROR]: Missing mandatory metadata - $i"
        err=12
      fi 
    done
  fi 

  recommended_list=("Sensor" "Relative_orbit_number" "Mode" "Orbit_direction" "Antenna_side" "Polarisation" "Main_reference" "Applied_algorithm_description" "Software_version" "Date_of_production" "Service_used_for_generation" "Map_of_LOS_vector" "Product_size" "Geographic_CS_type_code" "Value_unit")

  if [ $ed != "DEM_RADAR_GEOMETRY" -a $ed != "LOOKUP_TABLE_RADAR2GROUND_COORDINATES" ]; then
  for i in ${recommended_list[@]}; do 
    recommended_tmp=$(sed -n "s|^$i: \(.*\)|\1|gp" < $meta)
    if [ -z "$recommended_tmp" ]; then
        echo "[WARNING]: Missing recommended metadata - $i"
      fi 
  done
  fi

  [[ -z "$err" ]] || error $err "Parameter missing"
  
  echo "****************************************"
  echo " "

}

function name_check(){

  meta=$1
  echo "****************************************"
  #echo "Checking $meta file..."

  ed=$(sed -n "s|^DDSS_ID: \(.*\)|\1|gp" < $meta)
  prodID=$(sed -n "s|^Product_ID: \(.*\)|\1|gp" < $meta)

  echo "Product_ID: $prodID"
  echo "Data Type: $ed"
  echo " "
  IFS='_' read -r -a ppID <<< "$prodID"

  case $ed in

    "WRAPPED_INTERFEROGRAM")
      DataType="InW"
      ;;

    "UNWRAPPED_INTERFEROGRAM")
      DataType="InU"
      ;;

      "SPATIAL_COHERENCE")
      DataType="Coh"
      ;;

    "LOS_DISPLACEMENT_TIMESERIES")
      DataType="DTSLOS"
      ;;

    "MAP_OF_LOS_VECTOR")
      DataType="CosNEU"
      ;;

    "INTERFEROGRAM_APS_GLOBAL_MODEL")
      DataType="APS"
      ;;

    "LOOKUP_TABLE_RADAR2GROUND_COORDINATES")
      DataType="LUT"
      ;;

    "DEM_RADAR_GEOMETRY")
      DataType="DEM"
      ;;

      *)
      error 13 "Unrecognized Data Type"
      ;;

  esac

  if [ "${ppID[0]}" != "$DataType" ]; then
   echo "[ERROR]: Data Type must be $DataType - Actual value is: ${ppID[0]}"
   err=13
  fi
  
  if [ "${ppID[1]}" != "CNRIREA" -a "${ppID[1]}" != "GDMCNRS" -a "${ppID[1]}" != "COMET" ]; then
   echo "[ERROR]: Data Provider not recognized - Actual value is: ${ppID[1]}"
   err=13
  fi
  
  [[ -z "$err" ]] || error $err "Wrong Product_ID"

  echo "****************************************"
  echo " "

}

VERSION=1.0

#Get command line arguments
[[ -z "$1" ]] && usage

#Default arguments
DRYRUN=false
REPLACE=false
DELETE=false
WMS=false
GEP_CATALOGUE="https://catalog.terradue.com/gep-epos/"
NETRCFILE="${0%.sh*}.credentials"

#Parse arguments
while [[ "$#" -gt 0 ]]; do
  case "$1" in
   --dry-run) DRYRUN=true; shift 1 ;;
   --replace) REPLACE=true; shift 1 ;;
   --delete) DELETE=true; shift 1 ;;
   --wms) WMS=true; shift 1 ;;
   -h | --help | --version | -v) usage; shift 1 ;;
   *)
     if [[ "${1:0:1}" != "-" && -z "$METADATA" ]]; then
       METADATA="$1"
       shift 1
     else
        error 2 "Unknown option: $1"
     fi
    ;;
  esac
done
[[ -e "$METADATA" ]] || error 10 "Input metadata file does not exists"

#Check netrc file and extract credentials (needed for old versions of cURL which do not understand custom netrc
if [ $DRYRUN != true ]; then
[[ -e "$NETRCFILE" ]] || error 11 "Credentials file $NETRCFILE does not exist. Please create one."
GEP_CATSERVER="${GEP_CATALOGUE#http://}"; GEP_CATSERVER="${GEP_CATSERVER#https://}"; GEP_CATSERVER="${GEP_CATSERVER%%/*}";
CURLCRED=`sed -n 's|machine '"$GEP_CATSERVER"' login \([^ ]*\) secret \([^ ]*\)|\1:\2|p' < $NETRCFILE`
[[ -z "$CURLCRED" ]] && error 11 "Credentials for $GEP_CATSERVER machine not found in credentials file. Something is wrong"
fi

#Check mandatory metadata fileds
meta_check $METADATA

#Check Product Name
name_check $METADATA

if $DELETE; then
  #pid=`tr -d '\015' < "$METADATA" | sed -n 's|^Product_ID: \(.*\)|\1|gp'`
  pid=`sed -n 's|^Product_ID: \(.*\)|\1|gp' < $METADATA`
  warning "Deleting product $pid"

  if $DRYRUN; then
    #Do nothing
    warning "Dry-run, not deleting any product... (curl -s -S -u $CURLCRED -X DELETE ${GEP_CATALOGUE}query?uid=$pid)"
    exit 0
  else
    curl -s -S -u $CURLCRED -X DELETE -H "Accept: application/xml" "${GEP_CATALOGUE}query?uid=$pid"
    exit $?
  fi
fi

#Retreive template
ed=`sed -n 's|^DDSS_ID: \(.*\)|\1|gp' < $METADATA`
if $WMS; then TEMPL="templates_wms"; else TEMPL="templates"; fi
TEMPLATE="${0%/*}/$TEMPL/$ed.template.xml"
[[ -e $TEMPLATE ]] || error 11 "Data type specified not supported (missing template)"

#Set temporary files
X="${0%.*}.tmp.xml"
M="${0%.*}.tmp.meta"
X="${METADATA}.tmp.xml"
M="${METADATA}.tmp.meta"

cp -f "$TEMPLATE" "$X"
tr -d '\015' < "$METADATA" > "$M"

#Adjust metadata
#Bounding_box (need to be provided in multiple formats)
bbf=`sed -n 's|^Bounding_box: \(.*\)|\1|gp' < $M`
if [[ -n $bbf ]]; then
  #Add BBOX in WTK and GML formats (if not already present)
  FILE_BBOX=($bbf)
  FILE_POLYGON_WKT="POLYGON((${FILE_BBOX[1]} ${FILE_BBOX[0]},${FILE_BBOX[1]} ${FILE_BBOX[2]},${FILE_BBOX[3]} ${FILE_BBOX[2]},${FILE_BBOX[3]} ${FILE_BBOX[0]},${FILE_BBOX[1]} ${FILE_BBOX[0]}))"
  FILE_POLYGON_GML="${FILE_BBOX[0]} ${FILE_BBOX[1]} ${FILE_BBOX[2]} ${FILE_BBOX[1]} ${FILE_BBOX[2]} ${FILE_BBOX[3]} ${FILE_BBOX[0]} ${FILE_BBOX[3]} ${FILE_BBOX[0]} ${FILE_BBOX[1]}"
  [[ -z "`sed -n 's|^Bounding_box_wkt: \(.*\)|\1|gp' < $M`" ]] && echo "Bounding_box_wkt: $FILE_POLYGON_WKT" >> "$M"
  [[ -z "`sed -n 's|^Bounding_box_gml: \(.*\)|\1|gp' < $M`" ]] && echo "Bounding_box_gml: $FILE_POLYGON_GML" >> "$M"
else
  #If the bounding box is not present, check if you have the WKT or the GML
  bbf="`sed -n 's|^Bounding_box_wkt: \(.*\)|\1|gp' < $M`"
  if [[ -n $bbf ]]; then

    #Convert WKT in BBOX
    bbf="${bbf##*(}"; bbf="${bbf%%)*}";
    FILE_BBOX=`echo -n "$bbf" | gawk 'BEGIN{FS=" ";RS=",";bottom=1000;top=-1000;left=1000;right=-1000}{if($1>right)right=$1;if($1<left)left=$1;if($2>top)top=$2;if($2<bottom)bottom=$2;}END{printf bottom" "left" "top" "right}'`

    #Close the POLYGON or GEOMETRY if it is not closed
    bbf=`echo -n "$bbf" | gawk 'BEGIN{FS=" ";RS=",";f=0;}{if(f==0){v=$0;f=1;printf $0;}else{printf ","$0;};if(v==$0){e=1}else{e=0}}END{if(e==0)printf ","v;}'`

    #Convert to GML
    FILE_POLYGON_GML=`echo "$bbf" | gawk 'BEGIN{FS=" ";RS=","}{printf $2" "$1" "}'`
    [[ -z "`sed -n 's|^Bounding_box_gml: \(.*\)|\1|gp' < $M`" ]] && echo "Bounding_box: $FILE_BBOX" >> "$M"	
    [[ -z "`sed -n 's|^Bounding_box_gml: \(.*\)|\1|gp' < $M`" ]] && echo "Bounding_box_gml: $FILE_POLYGON_GML" >> "$M"
  fi
fi

bbf="`sed -n 's|^Bounding_box_gml: \(.*\)|\1|gp' < $M`"
if [[ -n $bbf ]]; then
  #Set number of GML elements
  FILE_BBOX=($bbf)
  FILE_POLYGON_GML_N=$(( ${#FILE_BBOX[@]} / 2 ))
  echo "Bounding_box_gml_n: $FILE_POLYGON_GML_N" >> "$M"
fi

##Date (need to be provided as single element if start/stop date is provided
sd=`sed -n 's|^Date_of_measurement_start: \(.*\)|\1|gp' < $M`
ed=`sed -n 's|^Date_of_measurement_end: \(.*\)|\1|gp' < $M`
if [[ $sd==$ed ]]; then
  echo "Date_of_measurement: $sd" >> "$M"
else
  echo "Date_of_measurement: $sd/$ed" >> "$M"
fi

##Resolution (to be split into x/y)
sd=`sed -n 's|^Spatial_resolution: \(.*\)|\1|gp' < "$M"`
sed -i '/Spatial_resolution/d' "$M"
ed="${sd#*,}"; sd="${sd%%,*}"
echo "y_Ground_Spatial_Resolution: $ed" >> "$M"
echo "x_Ground_Spatial_Resolution: $sd" >> "$M" 

##Test EPSG
epsg=`sed -n 's|^Geographic_CS_type_code: \(.*\)|\1|gp' < "$M"`
codValue="${epsg#*_}"; codSp="${epsg%%_*}"
re='^[0-9]+$'
if ! [[ $codValue =~ $re ]] ; then
   echo "code_space: RADAR_GEOMETRY"
   codValue="RADAR_GEOMETRY"
   codSp=""
fi

if ! [[ $codSp == $re ]] ; then
   warning "Missing code_space. Assuming EPSG"
   codSp="EPSG"
fi

echo "code_space: $codSp"
echo "code_space: $codSp" >> "$M"
echo "code_value: $codValue"
echo "code_value: $codValue" >> "$M"
echo " "

#Remove empty lines before processing
sed -i '/^[[:space:]]*$/d' $M

#Insert metadata into the template
while read line; do
  [[ -z "$line" ]] && continue
  [[ "$line" =~ ^# ]] && continue
  lnv=${line#*: }
  lnf=${line%%: *}
  lnv2=$(echo $lnv | sed 's|\&|\\\&|g' )  #correctly handles special character "&" in the pasted string
  sed 's|{{'"$lnf"'}}|'"$lnv2"'|g' "$X" > $X.2
  diff $X $X.2 &>/dev/null
  if [[ $? == 0 ]]; then
    warning "IGNORED INPUT - Metadata not found in $TEMPLATE - $lnf"
  fi
  mv $X.2 $X
done < $M

rm -f $X.2
lnn=`sed -n 's|.*{{\([^}]*\)}}.*|\1,|p' $X | tr -d '\n'`
if [[ -n "$lnn" ]]; then
 echo " " 
 IFS=', ' read -r -a missing <<< "$lnn"
  for jj in ${missing[@]}; do
    warning "MISSING METADATA - Metadata not found in $METADATA - $jj"
  done
fi
sed -i 's|{{[^}]*}}||g' $X

#Remove empty tags fields and cleanup
sed -i -e ':loop' -e 's/<\([a-zA-Z0-9\:\_\-]*\)[^>]*>[ \t]*<\/\1>//g' -e 't loop' $X

#Post the metadata to the catalogue
if $DRYRUN; then
   if $WMS; then 
	mv -f "$X" "${METADATA%.*}.map.xml"
   else
	mv -f "$X" "${METADATA%.*}.xml"
   fi
else
  if $REPLACE; then
    #delete old entry
	pid=`sed -n 's|^Product_ID: \(.*\)|\1|gp' < $M`
	curl -s -S -u $CURLCRED -X DELETE -H "Accept: application/xml" "${GEP_CATALOGUE}query?uid=$pid"
  fi
  cp -f "$X" "${METADATA%.*}.xml"
  if $WMS; then cp -f "$X" "${METADATA%.*}.map.xml" ; fi
  curl -s -S -u $CURLCRED -X POST -H "Content-Type: application/atom+xml" -H "Accept: application/xml" -d@$X "$GEP_CATALOGUE"
  rm -f "$X"
fi

#Cleanup temporary files
rm -f "$M"

echo ""
echo "SUCCESS!"
