# DDSS and metadata for the EPOS TCS Satellite Data

This folder contains the product and metadata specifications of EPOS TCS Satellite Data (SatData). 

Metadata specifications are available [**here**](https://docs.google.com/spreadsheets/d/1ap2ymZFGz-zTiXsttUKfRKDFB5GIOjiRgXc2wnFjg4E/edit?usp=sharing)  according to the [OGC standard](https://docs.opengeospatial.org/is/10-157r4/10-157r4.html).

The [**EPOS_product_register.sh**](EPOS_product_register.sh) script allows importing and publishing SatData products into the TCS catalog.

**NOTE:** Please read the **[EPOS_product_register_GUIDE.md](EPOS_product_register_GUIDE.md)** for furhter information
